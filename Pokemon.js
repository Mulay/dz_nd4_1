'use strict';

class Pokemon {
    constructor (name, level) {
        this.name = name;
        this.level = level;
    }
    show(){
        console.log(`Покемон ${this.name}, уровень ${this.level}.`);
    }
    valueOf(){
        return this.level;
    }
}

class PokemonList extends Array {
    constructor (...args){super (...args)}
    add(name, level){
        this.push(new Pokemon(name, level));
    }
    show(){
        console.log(`Количество покемонов в группе: ${this.length}.`);
        this.forEach((pokemon)=> pokemon.show());
    }
    translation(name, pokemonList){
        let removeId;
        pokemonList.push(this.find((pokemon, i , arr) => {
            if (Object.is(pokemon.name, name)){
                removeId = i;
                return pokemon;
            }
        }));
        this.splice(removeId, 1);
    }
    max(){
        let maxLevel = Math.max(...this.valueOf());
        return this.find((pokemon) => Object.is(maxLevel, pokemon.level));
    }
}

let lost = new PokemonList(new Pokemon("Charmander", 7), new Pokemon("Butterfree", 10));
let found = new PokemonList(new Pokemon("Pidgeotto", 2), new Pokemon("Pikachu", 6), new Pokemon("Jigglypuff", 1));

lost.add("Bulbasaur", 11);
lost.add("Gloom", 3);

found.add("Psyduck", 9);
lost.show();
lost.translation("Bulbasaur", found);
found.show();
lost.show();
console.log(found.max());

